<p>Many modern programming languages utilize HTTP parameters to determine the content displayed on a web page. This approach speeds up the creation of dynamic web pages, reduces the overall script size, and simplifies the code. In these scenarios, parameters specify which resource is shown on the page. However, if these functionalities are not securely coded, attackers can manipulate the parameters to display the content of any local file on the hosting server, resulting in a Local File Inclusion (LFI) or Remote File Inclusion (RFI) vulnerability.</p>
<p>When we are working with edx-platform code base, we can see that many APIs are using parameters to determine what kind of data must be returned or how it should be formatted. If the parameter serialization is not done correctly, or input validation is skipped (or poorly designed), a local file inclusion can be carried out. Similarly to SQL injections, we must be careful with string concatenation and execution. Take the following example.</p>
<p>
    <pre>
        <code>
            import os
            from django.http import HttpResponse
            from django.views import View

            class GradeReports (View):
                """The view handles grade report downloads."""

                def get(self, request, *args, **kwargs):
                    """Get the grade report by its date and return its content."""

                    filename = request.GET.get('report-name')
                    file_path = os.path.join('/tmp/generated/', filename)

                    with open (file_path, 'r') as file:
                        data = file.read()

                    return HttpResponse (data)
        </code>
    </pre>
<p></p>
<p>The API above can return any file based on its name from the “/tmp/generated/“ directory, right? Well, yes — with an emphasis on <strong>any file</strong> from the given directory. If the report directory contains sensitive files, those could be returned too. However, the biggest issue is not such same-directory inclusion. Nothing prevents the attacker from doing path traversal and reading any arbitrary file that the user running the web app has access to.</p>
<p>Consider the following parameter for the endpoint:</p>
<blockquote>
<p><code>report-name="../../etc/shadow"</code></p>
</blockquote>
<p>If the web app is executed with root privileges, what would happen? The attacker would send a request to the API and could get the passwords on the system to crack them offline. Again, we are using Docker, so it doesn't really matter since SSH is turned off, right? Yes, you are right, though the attacker has arbitrary file access. Nothing prevents them to dump a .env file or similar. Therefore, it can get access to any or all the following:</p>
<ul>
<li>Secret key -- so they can do log in session hijacking, CSRF hijacking, and more</li>
<li>DB credentials -- so they have access to the database, user emails, passwords, PII, other service credentials, and more</li>
<li>Proprietary code -- like private git repository content (like theme repos)</li>
<li>SSH keys -- like private keys used to fetch theme repos</li>
</ul>
<p>Preventing the malicious access can be done case-by-case, depending on the code, but let's see how the previous access can be fixed.</p>
<ul>
<li>We could run <a href="https://en.wikipedia.org/wiki/Chroot" target="_blank" rel="noopener">chroot</a> on the report directory, which is not a code-level change, but almost a mandatory step</li>
<li>Validate the input parameters before concatenation</li>
<li>Validate file path after concatenation</li>
<li>Use absolute path over relative path</li>
</ul>
<p>After applying the code-level changes listed above, the API would look like the following.</p>
<p>
    <pre>
        <code>
            import os import re
            from django.http import HttpResponse
            from django.views import View

            class GradeReports(View):
                """The view handles grade report downloads"""
                
                def get(self, request, *args, **kwargs):
                    """Get the grade report by its date and return its content."""
                    
                    filename = request GET.get('report-name')

                    # Ensure that the user cannot provide other characters than english
                    # letters and numbers.
                    
                    if not re.match(r"^[a-zA-Z0-9]$", filename):
                        return HttpResponse("bad request")
                
                    # Concatenate the csv extension in the backend.
                    file_path = os.path.join('/tmp/generated/', f"{filename}.csv")
                    file_path = os.path.abspath(file_path)
                # Check the concatenated path. This is unnecessary at this point, so
                # left here for demonstrational purposes.
                
                if not re-match(r"^\/tmp\/generated\/[a-zA-Z0-9].csv$", file_path):
                    return HttpResponse("bad request")
                    
                with open (file_path, 'r') as file:
                    data = file.read()

                return HttpResponse()
        </code>
    </pre>
</p>
<p></p>
<p>Now that we altered the backend logic, the caller can only reach CSV files in the given directory. Path traversal is prevented, however, the user could still access any CSVs on the path. For this demo, it is acceptable, but in a real world example, we shouldn't allow that. The report name should be stored in a DB, and retrieved by a unique, not guessable ID, therefore the attacker has almost no chance to access other reports.</p>
<p>When the IDs are guessable, like incrementing numbers, that exposes an attack surface to <a href="https://www.imperva.com/learn/application-security/insecure-direct-object-reference-idor/#:~:text=Insecure%20direct%20object%20references%20(IDOR,users%20without%20proper%20access%20controls." target="_blank" rel="noopener">IDOR (Insecure Direct Object References)</a> attacks. These attacks are letting the bad guys iterating over resources by their ID and may retrieve sensitive information. But that's a topic we won't cover now. Just note that it's better to use something like a UUID instead of an auto-incrementing ID in such situation.</p>
<p>If you would like to check it out, how LFI can work in practice, you can download a <a href="https://drive.google.com/file/d/1FBmv0ND_03G2B5UPU2vfTUVQn_D5FbE_/view?usp=drive_link" target="_blank" rel="noopener">pre-made Django project</a> from our Google Drive and practice. You will find the problem, hints, and solution. It is all about what we discussed above.</p>
<p>As a closing thought, the parameter names and path variables could be vulnerable to path traversal, however, Django is trying to do its best to keep away that issue from us.</p>